INSERT INTO `user` (`id`,`name`,`lastname`,`surname`,`telephone`,`mail`) VALUES (1,'Antoxa90','Victorovich','Missa','+375445647802','missa.anton@mail.ru');
INSERT INTO `user` (`id`,`name`,`lastname`,`surname`,`telephone`,`mail`) VALUES (2,'Andrey','Pavlovich','Sokolov','+375291234567','abcde@mail.ru');
INSERT INTO `user` (`id`,`name`,`lastname`,`surname`,`telephone`,`mail`) VALUES (3,'Igor','Alerseevich','Orlov','+375441239567','mymail@mail.ru');

INSERT INTO `demesne` (`id`,`id_type_Demesne`,`cost_toBuy`,`cost_perDay`,`id_user`,`photo`,`description`) VALUES (1,1,7000,200,2,'img_1.jpg','���������� ����������� �������');
INSERT INTO `demesne` (`id`,`id_type_Demesne`,`cost_toBuy`,`cost_perDay`,`id_user`,`photo`,`description`) VALUES (2,1,5000,150,2,'img_2.jpg','����������� �������');
INSERT INTO `demesne` (`id`,`id_type_Demesne`,`cost_toBuy`,`cost_perDay`,`id_user`,`photo`,`description`) VALUES (3,2,10000,250,3,'img_5.jpg','���������� ����������� �������');

INSERT INTO `order` (`id`,`id_user`,`id_demesne`,`id_type_of_order`,`days`,`status`) VALUES (1,2,1,1,5,1);
INSERT INTO `order` (`id`,`id_user`,`id_demesne`,`id_type_of_order`,`days`,`status`) VALUES (2,1,1,1,6,0);
INSERT INTO `order` (`id`,`id_user`,`id_demesne`,`id_type_of_order`,`days`,`status`) VALUES (3,2,2,1,5,0);

INSERT INTO `reply` (`id`,`id_demesne`,`id_user`,`text`,`likes`) VALUES (1,1,1,'�������� ����, ������� ������������. ����� ����������, ���� ���� �������������',0);
INSERT INTO `reply` (`id`,`id_demesne`,`id_user`,`text`,`likes`) VALUES (2,1,2,'������� ����� ������������, ���� �������. �������� �����������, ��� ����� ��� ��������� �� ������� ���� ������������.',0);
INSERT INTO `reply` (`id`,`id_demesne`,`id_user`,`text`,`likes`) VALUES (3,2,3,'�������� ����, �������� �����������, � ����� ���� ������� � ���',0);

INSERT INTO `type_demesne` (`id`,`name`) VALUES (1,'�������');
INSERT INTO `type_demesne` (`id`,`name`) VALUES (2,'�������');
INSERT INTO `type_demesne` (`id`,`name`) VALUES (3,'���������� ���');

INSERT INTO `type_of_order` (`id`,`name`) VALUES (1,'Rent');
INSERT INTO `type_of_order` (`id`,`name`) VALUES (2,'Buy');
INSERT INTO `type_of_order` (`id`,`name`) VALUES (3,'Sell');

INSERT INTO `authorization` (`id`,`group`,`id_user`,`login`,`password`) VALUES (1,'ROLE_ADMIN',1,'Antoxa90','7c222fb2927d828af22f592134e8932480637c0d');
INSERT INTO `authorization` (`id`,`group`,`id_user`,`login`,`password`) VALUES (2,'ROLE_USER',2,'Andrey','caa70946d8da3b59d1e0e798712934907f004695');
INSERT INTO `authorization` (`id`,`group`,`id_user`,`login`,`password`) VALUES (3,'ROLE_USER',3,'Igor11','7c4a8d09ca3762af61e59520943dc26494f8941b');