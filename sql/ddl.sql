CREATE TABLE authorization
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `group` VARCHAR(50),
    id_user INT NOT NULL,
    login VARCHAR(50),
    password VARCHAR(50)
);
CREATE TABLE demesne
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_type_Demesne INT DEFAULT 0 NOT NULL,
    cost_toBuy INT DEFAULT 0 NOT NULL,
    cost_perDay INT DEFAULT 0 NOT NULL,
    id_user INT DEFAULT 0 NOT NULL,
    photo VARCHAR(255) DEFAULT '0',
    description LONGTEXT NOT NULL,
    access INT DEFAULT '1'
);
CREATE TABLE likes
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_reply INT NOT NULL,
    id_user INT NOT NULL
);
CREATE TABLE `order`
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_user INT DEFAULT 0 NOT NULL,
    id_demesne INT DEFAULT 0 NOT NULL,
    id_type_of_order INT DEFAULT 0 NOT NULL,
    days INT DEFAULT 0 NOT NULL,
    status INT DEFAULT 0
);
CREATE TABLE reply
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_demesne INT DEFAULT 0 NOT NULL,
    id_user INT DEFAULT 0 NOT NULL,
    text LONGTEXT NOT NULL,
    likes INT
);
CREATE TABLE type_demesne
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) DEFAULT '0' NOT NULL
);
CREATE TABLE type_of_order
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) DEFAULT '0' NOT NULL
);
CREATE TABLE user
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) DEFAULT '0' NOT NULL,
    lastname VARCHAR(50) DEFAULT '0' NOT NULL,
    surname VARCHAR(50) DEFAULT '0' NOT NULL,
    telephone VARCHAR(50) DEFAULT '0',
    mail VARCHAR(50) DEFAULT '0' NOT NULL
);
ALTER TABLE authorization ADD FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX FK_authorization_user ON authorization (id_user);
ALTER TABLE demesne ADD FOREIGN KEY (id_type_Demesne) REFERENCES type_demesne (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE demesne ADD FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX FK_demesne_type_demesne ON demesne (id_type_Demesne);
CREATE INDEX FK_demesne_user ON demesne (id_user);
ALTER TABLE likes ADD FOREIGN KEY (id_reply) REFERENCES reply (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE likes ADD FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX FK_likes_reply ON likes (id_reply);
CREATE INDEX FK_likes_user ON likes (id_user);
ALTER TABLE `order` ADD FOREIGN KEY (id_demesne) REFERENCES demesne (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `order` ADD FOREIGN KEY (id_type_of_order) REFERENCES type_of_order (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `order` ADD FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX FK_order_demesne ON `order` (id_demesne);
CREATE INDEX FK_order_type_of_order ON `order` (id_type_of_order);
CREATE INDEX FK_order_user ON `order` (id_user);
ALTER TABLE reply ADD FOREIGN KEY (id_demesne) REFERENCES demesne (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE reply ADD FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE;
CREATE INDEX FK_reply_demesne ON reply (id_demesne);
CREATE INDEX FK_reply_user ON reply (id_user);
