package by.bsuir.demesne.util;

import by.bsuir.demesne.domain.bean.AuthorizationEntity;
import by.bsuir.demesne.service.AuthorizationService;
import by.bsuir.demesne.service.ServiceException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Scope(scopeName = "singleton")
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger LOGGER = LogManager.getLogger(UserDetailsServiceImpl.class);
    @Autowired
    private AuthorizationService authorizationService;

    private UserDetailsServiceImpl() {
    }

    public static UserDetailsService getInstance() {
        return SingletonHolder.INSTANCE;
    }


    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        AuthorizationEntity entity = null;
        try {
            entity = authorizationService.getUser(name);
        } catch (ServiceException e) {
            LOGGER.warn("ServiceException caught",e);
        }
        List<GrantedAuthority> authorityList = new ArrayList<>();
        authorityList.add(new SimpleGrantedAuthority(entity.getGroup().toString().toUpperCase()));
        return buildUserForAuthentication(entity, authorityList);

    }
    private User buildUserForAuthentication(AuthorizationEntity entity, List<GrantedAuthority> authorities) {
        return new User(entity.getLogin(),entity.getPassword(), authorities);
    }

    private static class SingletonHolder {
        private static final UserDetailsService INSTANCE = new UserDetailsServiceImpl();
    }

}
