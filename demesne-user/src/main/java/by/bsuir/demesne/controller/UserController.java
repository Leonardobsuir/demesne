package by.bsuir.demesne.controller;

import by.bsuir.demesne.domain.bean.*;
import by.bsuir.demesne.domain.enumeration.UserRole;
import by.bsuir.demesne.service.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class UserController {
    private static final Integer PAGE_SIZE = 4;
    private static final Integer DEFAULT_PAGE_NUMBER = 1;

    private static final String INDEX_PAGE = "index";
    private static final String DEMESNES_PAGE = "demesnes";
    private static final String ORDER_PAGE = "order";
    private static final String REPLY_PAGE = "reply";
    private static final String PERSONAL_PAGE = "personal";
    private static final String SELL_PAGE = "sell";
    private static final String LOGIN_PAGE = "login";
    private static final String SIGN_UP_PAGE = "signup";
    private static final String ERROR_404_PAGE = "404";
    private static final String ERROR_403_PAGE = "403";

    private static final String HOME_URL_MAPPING = "/";
    private static final String INDEX_URL_MAPPING = "/index";
    private static final String LOGIN_URL_MAPPING = "/login";
    private static final String DEMESNES_URL_MAPPING = "/demesnes";
    private static final String ORDER_URL_MAPPING = "/order";
    private static final String REPLIES_URL_MAPPING = "/replies";
    private static final String PERSONAL_URL_MAPPING = "/personal";
    private static final String SELL_URL_MAPPING = "/sell";
    private static final String SIGN_UP_URL_MAPPING = "/signup";
    private static final String ERROR_404_URL_MAPPING = "/error";
    private static final String ERROR_403_URL_MAPPING = "/403";
    private static final String LOGGER_MESSAGE = "ServiceException caught";

    private static final String ID_PARAM = "id" ;
    private static final String NAME_PARAM = "name";
    private static final String LOGIN_PARAM = "login";
    private static final String PASSWORD_PARAM = "password";
    private static final String SURNAME_PARAM = "surname";
    private static final String LASTNAME_PARAM = "lastname";
    private static final String TELEPHONE_PARAM = "telephone";
    private static final String MAIL_PARAM = "mail";


    private static final String PAGE_PAGINATION_PARAM = "page";
    private static final String PAGES_PAGINATION_PARAM = "pages";
    private static final String BEGIN_PAGINATION_PARAM = "beginIndex";
    private static final String END_PAGINATION_PARAM = "endIndex";
    private static final String CURRENT_PAGINATION_PARAM= "currentIndex";
    private static final String CONTENT_PAGINATION_PARAM= "content";

    private static final String LOGGER_SERVICE_EXCEPTION_MESSAGE = "ServiceException caught";
    private static final String LOGGER_EXCEPTION_MESSAGE = "Exception caught";

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);
    @Autowired
    DemesneService demesneService;
    @Autowired
    OrderService orderService;
    @Autowired
    AuthorizationService authorizationService;
    @Autowired
    UserService userService;
    @Autowired
    TypeOfOrderService typeOfOrderService;
    @Autowired
    ReplyService replyService;
    @Autowired
    ServletContext servletContext;


    @RequestMapping(value = {HOME_URL_MAPPING,INDEX_URL_MAPPING},method = RequestMethod.GET)
    public ModelAndView indexPage(){
        return new ModelAndView(INDEX_PAGE);
    }

    @RequestMapping(value = LOGIN_URL_MAPPING,method = RequestMethod.GET)
    public ModelAndView loginPage(){
        return new ModelAndView(LOGIN_PAGE);
    }

    @RequestMapping(value = SELL_URL_MAPPING,method = RequestMethod.GET)
    public ModelAndView sellPage(){
        return new ModelAndView(SELL_PAGE);
    }

    @RequestMapping(value = DEMESNES_URL_MAPPING,method = RequestMethod.GET)
    public ModelAndView demesnesPage(@RequestParam(value = PAGE_PAGINATION_PARAM, required = false) Integer pageNumber){
        ModelAndView modelAndView = new ModelAndView(DEMESNES_PAGE);
        try {
            Page<DemesneEntity> page = demesneService.readAll(pageNumber,PAGE_SIZE);
            paginate(modelAndView,page);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_SERVICE_EXCEPTION_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(value = PERSONAL_URL_MAPPING,method = RequestMethod.GET)
    public ModelAndView personalPage(@RequestParam(value = PAGE_PAGINATION_PARAM, required = false) Integer pageNumber){
        ModelAndView modelAndView = new ModelAndView(PERSONAL_PAGE);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        try {
            modelAndView.addObject("clientId", Integer.parseInt(authorizationService.getUser(username).getId().toString()));
            Page<OrderEntity> page = orderService.readAll(pageNumber,PAGE_SIZE);
            paginate(modelAndView,page);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_SERVICE_EXCEPTION_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(value = REPLIES_URL_MAPPING, method = RequestMethod.GET)
    public ModelAndView repliesPage(@RequestParam(value = PAGE_PAGINATION_PARAM, required = false) Integer pageNumber, @RequestParam(ID_PARAM) Long id){
        ModelAndView modelAndView = new ModelAndView(REPLY_PAGE);
        DemesneEntity entity = null;
        try {
            entity = demesneService.readById(id);
            Page<ReplyEntity> page = replyService.readAll(pageNumber,PAGE_SIZE);
            paginate(modelAndView, page);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_SERVICE_EXCEPTION_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        modelAndView.addObject("Demesne", entity);
        return modelAndView;
    }

    @RequestMapping(value = ORDER_URL_MAPPING,method = RequestMethod.GET)
    public ModelAndView orderPage(@RequestParam(value = ID_PARAM) Long id){
        ModelAndView modelAndView = new ModelAndView(ORDER_PAGE);
        DemesneEntity entity = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }

        try {
            entity = demesneService.readById(id);
            modelAndView.addObject("clientId", authorizationService.getUser(username).getId());
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_SERVICE_EXCEPTION_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }

        modelAndView.addObject("item", entity);
        return modelAndView;
    }

    @RequestMapping(value = SIGN_UP_URL_MAPPING,method = RequestMethod.GET)
    public ModelAndView signUpPage(){
        return new ModelAndView(SIGN_UP_PAGE);
    }

    @RequestMapping(value = ERROR_404_URL_MAPPING,method = RequestMethod.GET)
    public ModelAndView errorPage(){
        return new ModelAndView(ERROR_404_PAGE);
    }

    @RequestMapping(value = ERROR_403_URL_MAPPING,method = RequestMethod.GET)
    public ModelAndView error403Page(){
        return new ModelAndView(ERROR_403_PAGE);
    }

    @RequestMapping(value = ORDER_URL_MAPPING, method = RequestMethod.POST)
    public ModelAndView order(@RequestParam("clientId") Long clientId, @RequestParam("demesneId") Long demesneId,
                              @RequestParam("begin") String begin, @RequestParam("end") String end, @RequestParam("type") String typeOrder) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            DemesneEntity demesneEntity = demesneService.readById(demesneId);
            UserEntity client = userService.readById(clientId);
            Long idOrder = 0L;
            if(typeOrder.equals("Rent"))
                idOrder = 1L;
            if(typeOrder.equals("Buy"))
                idOrder = 2L;
            TypeOfOrderEntity type = typeOfOrderService.readById(idOrder);
            OrderEntity order = new OrderEntity();
            order.setId_user(client);
            order.setId_demesne(demesneEntity);
            order.setId_type_of_order(type);
            order.setStatus(0);
            if(typeOrder.equals("Rent")) {
                Date beginDate = dateFormat.parse(begin);
                Date endDate = dateFormat.parse(end);
                System.out.println(begin);
                System.out.println(end);
                Long days = (endDate.getTime() - beginDate.getTime()) / 86400000;
                order.setDays(Integer.parseInt(days.toString()));
                System.out.println(days);
            }
            else {
                order.setDays(0);
            }
            orderService.add(order);
            demesneEntity.setAccess(0);
            demesneService.update(demesneEntity);
        } catch (ServiceException|ParseException e) {
            LOGGER.warn(LOGGER_SERVICE_EXCEPTION_MESSAGE,e);
            System.out.println(e);
            return new ModelAndView(ERROR_404_PAGE);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE,exception);
            System.out.println(exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return new ModelAndView(INDEX_PAGE);
    }

    @RequestMapping(value = SIGN_UP_URL_MAPPING, method = RequestMethod.POST)
    public ModelAndView signUp(@RequestParam(NAME_PARAM) String  name,@RequestParam(LOGIN_PARAM) String  login,
                               @RequestParam(PASSWORD_PARAM) String  password,
                               @RequestParam(SURNAME_PARAM) String  surname, @RequestParam(LASTNAME_PARAM) String  lastname,
                               @RequestParam(TELEPHONE_PARAM) String  telephone, @RequestParam(MAIL_PARAM) String  mail){
        try {
            AuthorizationEntity entity = new AuthorizationEntity();
            entity = createUser(entity, name, login, password, surname, lastname, telephone, mail);
            authorizationService.add(entity);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_SERVICE_EXCEPTION_MESSAGE, e);
            System.out.println(e);
            return new ModelAndView(ERROR_404_PAGE);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            System.out.println(exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        ModelAndView modelAndView = new ModelAndView();
        RedirectView redirectView = new RedirectView(servletContext.getContextPath()+LOGIN_URL_MAPPING);
        modelAndView.setView(redirectView);
        return modelAndView;
    }

    @RequestMapping(value = SELL_URL_MAPPING, method = RequestMethod.POST)
    public ModelAndView addDemesne(@RequestParam("name") String name,
                                   @RequestParam("cost_toBuy") Integer cost_toBuy, @RequestParam("cost_perDay") Integer cost_perDay,
                                   @RequestParam("description") String description){
        try{
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principal = authentication.getPrincipal();
            String username;
            if (principal instanceof UserDetails) {
                username = ((UserDetails)principal).getUsername();
            } else {
                username = principal.toString();
            }

            UserEntity user = userService.readById(authorizationService.getUser(username).getId());
            DemesneEntity entity = createDemesne(new DemesneEntity(), name, cost_toBuy, cost_perDay, description, user);
            demesneService.add(entity);
        }catch (ServiceException e){
            System.out.println(e);
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            System.out.println(exception);
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return new ModelAndView(INDEX_PAGE);
    }

    @RequestMapping(value = REPLIES_URL_MAPPING, method = RequestMethod.POST)
    public ModelAndView addReply(@RequestParam("reply") String reply, @RequestParam("id") Long id) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principal = authentication.getPrincipal();
            String username;
            if (principal instanceof UserDetails) {
                username = ((UserDetails)principal).getUsername();
            } else {
                username = principal.toString();
            }

            UserEntity user = userService.readById(authorizationService.getUser(username).getId());
            DemesneEntity entity = demesneService.readById(id);
            ReplyEntity replyEntity = createReply(new ReplyEntity(), entity, user, reply);
            replyService.add(replyEntity);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_SERVICE_EXCEPTION_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return new ModelAndView(INDEX_PAGE);
    }

    private AuthorizationEntity createUser(AuthorizationEntity user, String  name, String  login, String password, String  surname, String lastname, String  telephone, String  mail){
        user.setLogin(login);
        user.setPassword(DigestUtils.sha1Hex(password));
        user.setGroup(UserRole.ROLE_USER);
        UserEntity client = new UserEntity();
        client.setName(name);
        client.setSurname(surname);
        client.setLastname(lastname);
        client.setTelephone(telephone);
        client.setMail(mail);
        user.setUserEntity(client);
        return user;
    }

    private DemesneEntity createDemesne(DemesneEntity entity , String name, Integer cost_toBuy, Integer cost_perDay, String description, UserEntity user){
        TypeDemesneEntity typeDemesneEntity = new TypeDemesneEntity();
        typeDemesneEntity.setName(name);
        entity.setTypeDemesneEntity(typeDemesneEntity);
        entity.setCost_toBuy(cost_toBuy);
        entity.setCost_perDay(cost_perDay);
        entity.setId_user(user);
        entity.setPhoto("0");
        entity.setDescription(description);
        entity.setAccess(1);
        return entity;
    }

    private ReplyEntity createReply(ReplyEntity replyEntity, DemesneEntity demesneEntity, UserEntity userEntity, String text) {
        replyEntity.setId_demesne(demesneEntity);
        replyEntity.setId_user(userEntity);
        replyEntity.setText(text);
        replyEntity.setLikes(0);
        return replyEntity;
    }

    private void paginate(ModelAndView modelAndView,Page page){
        int current = page.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, page.getTotalPages());
        modelAndView.addObject(PAGES_PAGINATION_PARAM, page);
        modelAndView.addObject(CONTENT_PAGINATION_PARAM, page.getContent());
        modelAndView.addObject(BEGIN_PAGINATION_PARAM, begin);
        modelAndView.addObject(END_PAGINATION_PARAM, end);
        modelAndView.addObject(CURRENT_PAGINATION_PARAM, current);
    }
}
