<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="user" />
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">

	<title><fmt:message key="user.login.title"/> </title>

	<link rel="shortcut icon" href="<c:url value="/pages/assets/images/gt_favicon.png"/>">

	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
	<link rel="stylesheet" href="<c:url value="/pages/assets/css/font-awesome.min.css"/>">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap-theme.css"/>" media="screen" >
	<link rel="stylesheet" href="<c:url value="/pages/assets/css/main.css"/>">

</head>

<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li class="active"><a href="${pageContext.request.contextPath}/index"><fmt:message key="menu.home"/> </a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="language"/> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="${pageContext.request.contextPath}/login?language=en">English</a></li>
							<li class="active"><a href="${pageContext.request.contextPath}/login?language=ru">Русский</a></li>
						</ul>
					</li>
					<li> <sec:authorize access="!isAuthenticated()">
						<a class="btn" href="<c:url value="/login" />"><fmt:message key="login.button"/> </a>
					</sec:authorize>
						<sec:authorize access="isAuthenticated()">
							<%-- <p>Ваш логин: <sec:authentication property="principal.username" /></p>--%>
							<a class="btn" href="<c:url value="/logout" />"><fmt:message key="logout.button" /></a>

						</sec:authorize></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">

		<ol class="breadcrumb">
			<li><a href="${pageContext.request.contextPath}/index"><fmt:message key="user.index.title"/> </a></li>
			<li class="active"><fmt:message key="user.login.title"/> </li>
		</ol>

		<div class="row">
			
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<header class="page-header">
					<h1 class="page-title"><fmt:message key="user.login.title"/> </h1>
				</header>
				
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center"><fmt:message key="login.message" /></h3>
							<p class="text-center text-muted"><fmt:message key="login.register.message"/> <a href="${pageContext.request.contextPath}/signup"><fmt:message key="register.title"/> </a></p>
							<hr>
							<c:url value="/j_spring_security_check" var="loginUrl" />
							<form name="login-form" action="${loginUrl}" method="post" >
								<div class="top-margin">
									<label><fmt:message key="user.login.title"/><span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="j_username">
								</div>
								<div class="top-margin">
									<label><fmt:message key="label.password"/> <span class="text-danger">*</span></label>
									<input type="password" class="form-control" name="j_password">
								</div>

								<hr>
								<div class="row">
									<div class="col-lg-4 text-right">
										<button class="btn btn-action" type="submit"><fmt:message key="user.login.title" /></button>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
				
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->


	<footer id="footer" class="top-space">
		<div class="footer1">
			<div class="container">
				<div class="row">

					<div class="col-md-3 widget">
						<h3 class="widget-title"><fmt:message key="footer.contact"/> </h3>
						<div class="widget-body">
							<p>
								<fmt:message key="footer.bsuir"/>
							<p class="text-left">
								<fmt:message key="footer.reserved" />.</p>
							<p class="text-left">

							</p>
							</p>
						</div>
					</div>

					<!--<div class="col-md-3 widget">
                        <h3 class="widget-title"> </h3>
                        <div class="widget-body">
                            <p class="follow-me-icons">
                                <a href="https://github.com/"><i class="fa fa-github fa-2"></i></a>
                            </p>
                        </div>
                    </div>-->

				</div>
			</div>
		</div>
	</footer>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="<c:url value="/pages/assets/js/headroom.min.js"/>"></script>
	<script src="<c:url value="/pages/assets/js/jQuery.headroom.min.js"/>"></script>
	<script src="<c:url value="/pages/assets/js/template.js"/>"></script>

</body>
</html>