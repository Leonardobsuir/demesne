<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="user" />
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title><fmt:message key="order.title" /> </title>

    <link rel="shortcut icon" href="<c:url value="/pages/assets/images/gt_favicon.png"/>">

    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/font-awesome.min.css"/>">

    <!-- Custom styles for our template -->
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap-theme.css"/>" media="screen" >
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/main.css"/>">
    <script src="<c:url value="/pages/assets/js/jquery-2.1.3.min.js"/>"></script>
    <script src="<c:url value="/pages/assets/js/script.js"/>"></script>
</head>

<body>
<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li class="active"><a href="${pageContext.request.contextPath}/index"><fmt:message key="menu.home"/> </a></li>
                <li> <sec:authorize access="!isAuthenticated()">
                    <a class="btn" href="<c:url value="/login" />"><fmt:message key="login.button"/> </a>
                </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <a class="btn" href="<c:url value="/logout" />"><fmt:message key="logout.button" /></a>

                    </sec:authorize></li>
            </ul>
        </div>
    </div>
</div>
<!-- /.navbar -->

<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">
    <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/index"><fmt:message key="user.index.title"/> </a></li>
        <li><a href="${pageContext.request.contextPath}/demesnes?page=1"><fmt:message key="demesnes.title"/> </a></li>
        <li class="active"><fmt:message key="order.title"/> </li>
    </ol>
    <br>
    <br>
    <div class="bg_content_btm">
        <div class="bg_content_top">
            <div class="bg_content_inner">

                <form name="cart_quantity" action="${pageContext.request.contextPath}/order" method="post">
                    <input type="hidden" name="clientId" value="${clientId}"/>
                    <input type="hidden" name="demesneId" value="${pageContext.request.getParameter("id")}"/>
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" style="">
                        <tbody>
                        <tr>
                            <td style="padding:0px 10px 0px 0px;">
                                <table border="0" width="100%" cellspacing="0" cellpadding="0" class="productListing">
                                    <tbody><tr>
                                        <td align="center" class="productListing-heading"></td>
                                        <td align="center" class="productListing-heading"></td>
                                    </tr>
                                    <tr class="productListing-even">

                                        <td class="productListing-data"><table border="0" cellspacing="2" cellpadding="2">  <tbody><tr>    <td class="productListing-data" align="center"><img src="<c:url value="/pages/assets/images/${item.photo}"/>" border="0" width="215" height="167"></a>

                                        </td>
                                            <td class="productListing-data" valign="middle">
                                                <p><b>${item.typeDemesneEntity.name}</b></p>

                                                <p><b>${item.description}</b></p>
                                                <p><b><fmt:message key="demesnes.cost_toBuy"/>: $${item.cost_toBuy}</b></p>
                                                <p><b><fmt:message key="demesnes.cost_perDay"/>: $${item.cost_perDay}</b></p>
                                                </a>
                                                <select class="form-control" name="type" id="type">
                                                    <option>Rent</option>
                                                    <option>Buy</option>
                                                </select>

                                            </td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        </td>
                                        <td id="dateRent" align="center" class="productListing-data" valign="middle"><label for="calendar-first"><fmt:message key="application.begin"/> </label><br>

                                            <input type="date" id="calendar-first" name="begin">
                                            <hr><label for="calendar-second"><fmt:message key="application.end"/> </label><br>
                                            <input type="date" id="calendar-second" name="end">
                                        </td>
                                    </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <hr>
                            <td><table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
                                <tbody><tr class="infoBoxContents">
                                    <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                            <hr>
                                            <td>
                                                <input type="submit" class="btn" name="Save" value="<fmt:message key="order.save"/> ">
                                            </td>
                                        </tr>
                                        </tbody></table></td>
                                </tr>
                                </tbody>
                            </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>

</div>
<footer id="footer" class="top-space">
    <div class="footer1">
        <div class="container">
            <div class="row">

                <div class="col-md-3 widget">
                    <h3 class="widget-title"><fmt:message key="footer.contact"/> </h3>
                    <div class="widget-body">
                        <p>
                            <fmt:message key="footer.bsuir"/>
                        <p class="text-left">
                            <fmt:message key="footer.reserved" />.</p>
                        <p class="text-left">

                        </p>
                        </p>
                    </div>
                </div>

                <!--<div class="col-md-3 widget">
                    <h3 class="widget-title"> </h3>
                    <div class="widget-body">
                        <p class="follow-me-icons">
                            <a href="https://github.com/"><i class="fa fa-github fa-2"></i></a>
                        </p>
                    </div>
                </div>-->

            </div>
        </div>
    </div>

</footer>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="<c:url value="/pages/assets/js/headroom.min.js"/>"></script>
<script src="<c:url value="/pages/assets/js/jQuery.headroom.min.js"/>"></script>
<script src="<c:url value="/pages/assets/js/template.js"/>"></script>
<script src="<c:url value="/pages/assets/js/date.js"/>"></script>
</html>