<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="user" />
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport"    content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">

  <title><fmt:message key="demesnes.title"/> </title>

  <link rel="shortcut icon" href="<c:url value="/pages/assets/images/gt_favicon.png"/>">

  <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
  <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
  <link rel="stylesheet" href="<c:url value="/pages/assets/css/font-awesome.min.css"/>">

  <!-- Custom styles for our template -->
  <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap-theme.css"/>" media="screen" >
  <link rel="stylesheet" href="<c:url value="/pages/assets/css/main.css"/>">

</head>

<body>
<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
  <div class="container">
    <div class="navbar-header">
      <!-- Button for smallest screens -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

    </div>
      <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav pull-right">
              <li class="active"><a href="${pageContext.request.contextPath}/index"><fmt:message key="menu.home"/> </a></li>
              <li> <sec:authorize access="!isAuthenticated()">
                  <a class="btn" href="<c:url value="/login" />"><fmt:message key="login.button"/> </a>
              </sec:authorize>
                  <sec:authorize access="isAuthenticated()">
                      <a class="btn" href="<c:url value="/logout" />"><fmt:message key="logout.button" /></a>

                  </sec:authorize></li>
          </ul>
      </div>
  </div>
</div>
<!-- /.navbar -->

<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">
    <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/index"><fmt:message key="user.index.title"/> </a></li>
        <li class="active"><fmt:message key="demesnes.title"/> </li>
    </ol>
    <br>
    <br>
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
    <thead>
    <hr>
    </thead>
        <br>
        <tfoot>
    <td colspan="8">
      <c:url var="firstUrl" value="/demesnes?page=1" />
      <c:url var="lastUrl" value="/demesnes?page=${pages.totalPages}" />
      <c:url var="prevUrl" value="/demesnes?page=${currentIndex - 1}" />
      <c:url var="nextUrl" value="/demesnes?page=${currentIndex + 1}" />
      <ul class="pagination">
          <c:choose>
              <c:when test="${currentIndex == 1}">
                  <li class="disabled"><a href="#">&lt;&lt;</a></li>
                  <li class="disabled"><a href="#">&lt;</a></li>
              </c:when>
              <c:otherwise>
                  <li><a href="${firstUrl}">&lt;&lt;</a></li>
                  <li><a href="${prevUrl}">&lt;</a></li>
              </c:otherwise>
          </c:choose>
          <c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
              <c:url var="pageUrl" value="/demesnes?page=${i}" />
              <c:choose>
                  <c:when test="${i == currentIndex}">
                      <li class="active"><a href="${pageUrl}"><c:out value="${i}" /></a></li>
                  </c:when>
                  <c:otherwise>
                      <li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
                  </c:otherwise>
              </c:choose>
          </c:forEach>
          <c:choose>
              <c:when test="${currentIndex == pages.totalPages}">
                  <li class="disabled"><a href="#">&gt;</a></li>
                  <li class="disabled"><a href="#">&gt;&gt;</a></li>
              </c:when>
              <c:otherwise>
                  <li><a href="${nextUrl}">&gt;</a></li>
                  <li><a href="${lastUrl}">&gt;&gt;</a></li>
              </c:otherwise>
          </c:choose>
      </ul>
     </td>
    </tfoot>
    <tbody>
    <tr>
    <c:forEach var="item" items="${content}">
        <td align="left" class="smallText" valign="top" >
            <div class="box_new_mdl">
                <div  class="box_new_btm">
                    <div  class="box_new_top">
                        <div class="box_new_content">
                            <c:if test="${item.access == 1}">
                            <div><a class="product_name" style="display:block;" href="${pageContext.request.contextPath}/order?id=${item.id}">${item.typeDemesneEntity.name}</a></div>
                            <div style="line-height:9px">&nbsp;</div>
                            <a href="${pageContext.request.contextPath}/replies?page=1&id=${item.id}"><img src="<c:url value="/pages/assets/images/${item.photo}"/>" border="0" width="215" height="167" style="border:0px solid #E8E8E8;"></a><img src="images/spacer.gif" border="0" alt="" width="1" height="1" class="img_border">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:12px 0 0 15px;">
                                <tr>
                                    <td style="padding-top:10px;"  valign="top" align="left"><img src="<c:url value="/pages/assets/images/info.jpg"/>" border="0" alt="${item.description}" width="33" height="34"></td>
                                    <td style="padding-top:10px;width:89px; overflow:hidden"  valign="top" align="left"><span class="Price2" style="margin-left:2px;">$${item.cost_perDay}</span><div style="color:#888888; font-size:12px"><fmt:message key="per.day"/></div></td>
                                    <td style=""  valign="top" align="left"><a href="${pageContext.request.contextPath}/order?id=${item.id}"><img src="<c:url value="/pages/assets/images/add.jpg"/>" border="0" alt="Select this demesne" title="Select this demesne" width="71" height="58"></a></td>
                                </tr>
                            </table>
                            </c:if>
                            <c:if test="${item.access != 1}">
                                <div><a class="product_name" style="display:block;" href="${pageContext.request.contextPath}/demesnes?page=1#">${item.typeDemesneEntity.name}</a></div>
                                <div style="line-height:9px">&nbsp;</div>
                                <a href="${pageContext.request.contextPath}/replies?page=1&id=${item.id}"><img src="<c:url value="/pages/assets/images/${item.photo}"/>" border="0" width="215" height="167" style="border:0px solid #E8E8E8;"></a><img src="images/spacer.gif" border="0" alt="" width="1" height="1" class="img_border">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:12px 0 0 15px;">
                                    <tr>
                                        <td style="padding-top:10px;"  valign="top" align="left"><img src="<c:url value="/pages/assets/images/info.jpg"/>" border="0" alt="" width="33" height="34"></td>
                                        <td style="padding-top:10px;width:89px; overflow:hidden"  valign="top" align="left"><span class="Price2" style="margin-left:2px;">$${item.cost_perDay}</span><div style="color:#888888; font-size:12px"><fmt:message key="per.day"/></div></td>
                                        <td style=""  valign="top" align="left"><a href="${pageContext.request.contextPath}/demesnes?page=1#"><img src="<c:url value="/pages/assets/images/add.jpg"/>" border="0" alt="Unavailable" title="Unavailable" width="71" height="58"></a></td>
                                    </tr>
                                </table>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </c:forEach>
    </tr>
    </tbody>

  </table>

</div>	<!-- /container -->


<footer id="footer" class="top-space">
    <div class="footer1">
        <div class="container">
            <div class="row">

                <div class="col-md-3 widget">
                    <h3 class="widget-title"><fmt:message key="footer.contact"/> </h3>
                    <div class="widget-body">
                        <p>
                            <fmt:message key="footer.bsuir"/>
                        <p class="text-left">
                            <fmt:message key="footer.reserved" />.</p>
                        <p class="text-left">

                        </p>
                        </p>
                    </div>
                </div>

                <!--<div class="col-md-3 widget">
                    <h3 class="widget-title"> </h3>
                    <div class="widget-body">
                        <p class="follow-me-icons">
                            <a href="https://github.com/"><i class="fa fa-github fa-2"></i></a>
                        </p>
                    </div>
                </div>-->

            </div>
        </div>
    </div>

</footer>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="<c:url value="/pages/assets/js/headroom.min.js"/>"></script>
<script src="<c:url value="/pages/assets/js/jQuery.headroom.min.js"/>"></script>
<script src="<c:url value="/pages/assets/js/template.js"/>"></script>
</body>
</html>