var calendar1 = document.getElementById("calendar-first");
var calendar2 = document.getElementById("calendar-second");

function rightDate() {
  var calendar1 = document.getElementById("calendar-first");
  var calendar2 = document.getElementById("calendar-second");
  var calendFirstValue = calendar1.value;
  calendar2.setAttribute("min", get(calendFirstValue, 1));
}

function get(data, day) {
  data = data.split('-');
  console.log(data);

  var date = new Date();

  date.setDate(parseInt(data[2]));
  date.setMonth(parseInt(data[1]));
  date.setFullYear(parseInt(data[0]));
  date.setDate(date.getDate() + day);
  var month = date.getMonth();
  if(parseInt(month) == 0){
    month = 12;
  }
  var year = date.getFullYear();
  console.log(date.getDate() + "  " + month + "  " + year);

  if(parseInt(month) < 10) {
    if(date.getDate() < 10){
      var newDate = [date.getFullYear(), "0" + month, "0" + date.getDate()];
    } else {
      var newDate = [date.getFullYear(), "0" + month, date.getDate()];
    }
    if(date.getDate() == 1 && month == 1){
      var newDate = [date.getFullYear() + 1, "0" + month, "0" + date.getDate()];
    }
  } else {
    var newDate = [date.getFullYear(), month, date.getDate()];
  }
  return newDate.join('-');
}

$("#calendar-first").change(rightDate);