<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="user" />
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <title><fmt:message key="user.index.title"/> </title>

    <link rel="shortcut icon" href="<c:url value="/pages/assets/images/gt_favicon.png"/>">

    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/font-awesome.min.css"/>">

    <!-- Custom styles for our template -->
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap-theme.css"/>" media="screen" >
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/main.css"/>">

</head>

<body class="home">
<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <li class="active"><a href="${pageContext.request.contextPath}/admin/">Admin Home</a></li>
                </sec:authorize>
                <li class="active"><a href="${pageContext.request.contextPath}/"><fmt:message key="menu.home"/></a></li>
                <%--<li class="action"><a href="${pageContext.request.contextPath}/replies?page=1"><fmt:message key="reply.title"/> </a></li>--%>

                <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="demesnes.title"/><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="${pageContext.request.contextPath}/demesnes?page=1"><fmt:message key="demesnes.title.rent"/> </a></li>
                        <li><a href="${pageContext.request.contextPath}/sell"><fmt:message key="demesnes.title.sell"/></a></li>
                    </ul>
                </li>
                </sec:authorize>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="language"/><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="${pageContext.request.contextPath}/index?language=en">English</a></li>
                        <li class="active"><a href="${pageContext.request.contextPath}/index?language=ru">Русский</a></li>
                    </ul>
                </li>
                <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')">
                    <li><a href="${pageContext.request.contextPath}/personal?page=1"><fmt:message key="personal"/></a></li>
                </sec:authorize>
                <li> <sec:authorize access="!isAuthenticated()">
                    <a class="btn" href="<c:url value="/login" />"><fmt:message key="login.button"/> </a>
                </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <a class="btn" href="<c:url value="/logout" />"><fmt:message key="logout.button" /></a>

                    </sec:authorize></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
<!-- /.navbar -->
<header id="head">
    <div class="container">
        <div class="row">

        </div>
    </div>
</header>


<div class="container">

    <h2 class="text-center top-space"><fmt:message key="hello"/> </h2>
    <br>
    <div class="row">
        <div class="col-sm-12 text-center" >
            <h3><fmt:message key="index.message"/> <a href="${pageContext.request.contextPath}/demesnes?page=1"><fmt:message key="demesnes.title"/></a> </h3>
        </div>

    </div> <!-- /row -->

</div>	<!-- /container -->

<section id="social">
    <div class="container">
        <div class="wrapper clearfix">
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style">
                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                <a class="addthis_button_tweet"></a>
                <a class="addthis_button_linkedin_counter"></a>
                <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
            </div>
            <!-- AddThis Button END -->
        </div>
    </div>
</section>
<!-- /social links -->

<footer id="footer" class="top-space">
    <div class="footer1">
        <div class="container">
            <div class="row">

                <div class="col-md-3 widget">
                    <h3 class="widget-title"><fmt:message key="footer.contact"/> </h3>
                    <div class="widget-body">
                        <p>
                            <fmt:message key="footer.bsuir"/>
                        <p class="text-left">
                        <fmt:message key="footer.reserved" />.</p>
                        <p class="text-left">

                        </p>
                        </p>
                    </div>
                </div>

                <!--<div class="col-md-3 widget">
                    <h3 class="widget-title"> </h3>
                    <div class="widget-body">
                        <p class="follow-me-icons">
                            <a href="https://github.com/"><i class="fa fa-github fa-2"></i></a>
                        </p>
                    </div>
                </div>-->

            </div>
        </div>
    </div>

</footer>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="<c:url value="/pages/assets/js/headroom.min.js"/>"></script>
<script src="<c:url value="/pages/assets/js/jQuery.headroom.min.js"/>"></script>
<script src="<c:url value="/pages/assets/js/template.js"/>"></script>
</body>
</html>