<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="user" />


<html>
<head>
    <title><fmt:message key="403.title"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link rel="stylesheet" media="screen" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">

    <link rel="stylesheet" media="screen" href="<c:url value="/pages/assets/css/bootstrap-theme.css"/>">

    <!-- Bootstrap Admin Theme -->
    <link rel="stylesheet" media="screen" href="<c:url value="/pages/assets/css/bootstrap-admin-theme.css"/>">
    <link rel="stylesheet" media="screen" href="<c:url value="/pages/assets/css/bootstrap-admin-theme-change-size.css"/>">

    <!-- Bootstrap Error Page -->
    <link rel="stylesheet" media="screen" href="<c:url value="/pages/assets/css/bootstrap-error-page.css"/>">
</head>
<body>

<!-- content -->
<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-lg-12">
            <div class="centering text-center error-container">
                <div class="text-center">
                    <h2 class="without-margin"><fmt:message key="403.message.p1"/> <span class="text-warning"><big>403</big></span>  <fmt:message key="403.message.p2"/>.</h2>
                    <h4 class="text-warning"><fmt:message key="403.title"/> </h4>
                </div>
                <div class="text-center">
                    <h3><small><fmt:message key="403.option"/> </small></h3>
                </div>
                <hr>
                <ul class="pager">
                    <li><a href="${pageContext.request.contextPath}/index"><fmt:message key="user.index.title" /></a></li>
                    <li><a href="${pageContext.request.contextPath}/login"><fmt:message key="user.login.title" /></a></li>
                    <li><a href="${pageContext.request.contextPath}/signup"><fmt:message key="signup.title" /></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<c:url value="/pages/assets/js/bootstrap-admin-theme-change-size.js"/>"></script>
</body>
</html>
