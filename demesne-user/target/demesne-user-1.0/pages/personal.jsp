<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="user" />
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport"    content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">

  <title><fmt:message key="personal"/> </title>

  <link rel="shortcut icon" href="<c:url value="/pages/assets/images/gt_favicon.png"/>">

  <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
  <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
  <link rel="stylesheet" href="<c:url value="/pages/assets/css/font-awesome.min.css"/>">

  <!-- Custom styles for our template -->
  <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap-theme.css"/>" media="screen" >
  <link rel="stylesheet" href="<c:url value="/pages/assets/css/main.css"/>">

</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top headroom" >
  <div class="container">
    <div class="navbar-header">
      <!-- Button for smallest screens -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav pull-right">
        <li class="active"><a href="${pageContext.request.contextPath}/index"><fmt:message key="menu.home"/> </a></li>
        <li> <sec:authorize access="!isAuthenticated()">
          <a class="btn" href="<c:url value="/login" />"><fmt:message key="login.button"/> </a>
        </sec:authorize>
          <sec:authorize access="isAuthenticated()">
            <a class="btn" href="<c:url value="/logout" />"><fmt:message key="logout.button" /></a>

          </sec:authorize></li>
      </ul>
    </div>
  </div>
</div>

<header id="head" class="secondary"></header>

<div class="container">
  <ol class="breadcrumb">
    <li><a href="${pageContext.request.contextPath}/index"><fmt:message key="user.index.title"/> </a></li>
    <li class="active"><fmt:message key="personal"/> </li>
  </ol>
  <br>
  <br>
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <table class="table">
      <thead>
      <tr>
        <th>#</th>
        <th><fmt:message key="order.client"/></th>
        <th><fmt:message key="orders.demesne"/></th>
        <th><fmt:message key="orders.type"/></th>
        <th><fmt:message key="demesnes.description"/></th>
        <th><fmt:message key="order.price"/></th>
        <th><fmt:message key="orders.status"/></th>
      </tr>
      </thead>
      <tfoot>
      <td colspan="8">
        <c:url var="firstUrl" value="/personal?page=1" />
        <c:url var="lastUrl" value="/personal?page=${pages.totalPages}" />
        <c:url var="prevUrl" value="/personal?page=${currentIndex - 1}" />
        <c:url var="nextUrl" value="/personal?page=${currentIndex + 1}" />
        <ul class="pagination">
          <c:choose>
            <c:when test="${currentIndex == 1}">
              <li class="disabled"><a href="#">&lt;&lt;</a></li>
              <li class="disabled"><a href="#">&lt;</a></li>
            </c:when>
            <c:otherwise>
              <li><a href="${firstUrl}">&lt;&lt;</a></li>
              <li><a href="${prevUrl}">&lt;</a></li>
            </c:otherwise>
          </c:choose>
          <c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
            <c:url var="pageUrl" value="/personal?page=${i}" />
            <c:choose>
              <c:when test="${i == currentIndex}">
                <li class="active"><a href="${pageUrl}"><c:out value="${i}" /></a></li>
              </c:when>
              <c:otherwise>
                <li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
              </c:otherwise>
            </c:choose>
          </c:forEach>
          <c:choose>
            <c:when test="${currentIndex == pages.totalPages}">
              <li class="disabled"><a href="#">&gt;</a></li>
              <li class="disabled"><a href="#">&gt;&gt;</a></li>
            </c:when>
            <c:otherwise>
              <li><a href="${nextUrl}">&gt;</a></li>
              <li><a href="${lastUrl}">&gt;&gt;</a></li>
            </c:otherwise>
          </c:choose>
        </ul>
      </td>
      </tfoot>
      <tbody>
      <c:forEach var="item" items="${content}">
        <c:if test="${item.id_user.id eq clientId}">
        <tr>
          <td>${item.id}</td>
          <td>${item.id_user.name}</td>
          <td>${item.id_demesne.typeDemesneEntity.name}</td>
          <td>${item.id_demesne.description}</td>
          <td>${item.id_type_of_order.name}</td>
          <td>
            <c:if test="${item.days == 0}">${item.id_demesne.cost_toBuy}</c:if>
            <c:if test="${item.days != 0}">${item.id_demesne.cost_perDay * item.days}</c:if>
          </td>
          <td>${item.status}</td>
        </tr>
        </c:if>
      </c:forEach>
      </tbody>
    </table>
  </div>
</div>

</div>	<!-- /container -->

<footer id="footer" class="top-space">
  <div class="footer1">
    <div class="container">
      <div class="row">

        <div class="col-md-3 widget">
          <h3 class="widget-title"><fmt:message key="footer.contact"/> </h3>
          <div class="widget-body">
            <p>
              <fmt:message key="footer.bsuir"/>
            <p class="text-left">
              <fmt:message key="footer.reserved" />.</p>
            <p class="text-left">

            </p>
            </p>
          </div>
        </div>

        <!--<div class="col-md-3 widget">
            <h3 class="widget-title"> </h3>
            <div class="widget-body">
                <p class="follow-me-icons">
                    <a href="https://github.com/"><i class="fa fa-github fa-2"></i></a>
                </p>
            </div>
        </div>-->

      </div>
    </div>
  </div>

</footer>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="<c:url value="/pages/assets/js/headroom.min.js"/>"></script>
<script src="<c:url value="/pages/assets/js/jQuery.headroom.min.js"/>"></script>
<script src="<c:url value="/pages/assets/js/template.js"/>"></script>
<script src="<c:url value="/pages/assets/js/date.js"/>"></script>
</html>

