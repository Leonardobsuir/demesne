<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="change_demesne.title"/></title>
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/cover.css"/>">
    
</head>
<body>
<div class="site-wrapper">
    <div class="site-wrapper-inner">
        <div class="cover-container">
            <div class="masthead clearfix">
                <div class="inner">
                    <ul class="nav masthead-nav">
                        <li><a href="${pageContext.request.contextPath}/admin"><fmt:message key="menu.admin.home"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/logout"><fmt:message key="menu.logout"/></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <div class="signup-form">
                    <form class="form-signin" action="${pageContext.request.contextPath}/edit_demesne" method="POST" role="form">
                        <div class="form-group">
                            <label for="cost_toBuy" class="col-sm-3 control-label"><fmt:message key="demesnes.cost_toBuy"/></label>
                            <div class="col-sm-9">
                                <input class="form-control" type="number" id="cost_toBuy" name ="cost_toBuy" placeholder="<fmt:message key="demesnes.cost_toBuy"/>"  value="${demesneEntity.cost_toBuy}"  required="required"
                                       min="1" title="<fmt:message key="add_demesne.costBuy.message"/>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cost_perDay" class="col-sm-3 control-label"><fmt:message key="demesnes.cost_perDay"/></label>
                            <div class="col-sm-9">
                                <input class="form-control" type="number" id="cost_perDay" name ="cost_perDay" placeholder="<fmt:message key="demesnes.cost_perDay"/>" value="${demesneEntity.cost_perDay}"  required="required"
                                       min="1" title="<fmt:message key="add_demesne.costDay.message"/>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-3 control-label"><fmt:message key="add_demesne.description"/></label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" id="description" name ="description" placeholder="<fmt:message key="add_demesne.description"/>" value="${demesneEntity.description}"  required="required"
                                       pattern="[a-zA-Zа-яА-Я0-9 ]{1,63}" title="<fmt:message key="add_demesne.description.message"/>"/>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="${demesne.id}">
                        <input type="submit" name="Save" value="<fmt:message key="save"/>"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-2 sidebar">
            <div class="inner">
                <h3><fmt:message key="admin.menu"/></h3>
                <ul class="nav nav-list">
                    <li><a href="${pageContext.request.contextPath}/show_demesnes?page=1"><fmt:message key="admin.menu.demesne"/> </a></li>
                    <li><a href="${pageContext.request.contextPath}/add_demesne"><fmt:message key="admin.menu.demesne.add"/></a></li>
                    <li><a href="${pageContext.request.contextPath}/show_clients?page=1"><fmt:message key="admin.menu.client"/> </a></li>
                    <li><a href="${pageContext.request.contextPath}/show_orders?page=1"><fmt:message key="admin.menu.orders.show"/></a></li>

                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
