<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<html>
<head>
    <title><fmt:message key="admin.title"/> </title>
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/cover.css"/>">
</head>
<body>
<div class="site-wrapper">
    <div class="site-wrapper-inner">
        <div class="cover-container">
            <div class="masthead clearfix">
                <div class="inner">
                    <ul class="nav masthead-nav">
                        <li><a href="http://localhost:8080/demesne/index"><fmt:message key="label.site"/></a> </li>
                        <li><a href="${pageContext.request.contextPath}/admin"><fmt:message key="menu.admin.home"/></a> </li>
                        <li><a href="${pageContext.request.contextPath}/admin?language=ru"><fmt:message key="menu.ru"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/admin?language=en"><fmt:message key="menu.en"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/logout"><fmt:message key="menu.logout"/></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <p class="lead"><fmt:message key="admin.hello"/></p>
                </div>
        </div>

        <div class="col-sm-3 col-md-2 sidebar">
            <div class="inner">
            <h3><fmt:message key="admin.menu"/></h3>
            <ul class="nav nav-list">
                <li><a href="${pageContext.request.contextPath}/show_demesnes?page=1"><fmt:message key="admin.menu.demesne"/> </a></li>
                <li><a href="${pageContext.request.contextPath}/add_demesne"><fmt:message key="admin.menu.demesne.add"/></a></li>
                <li><a href="${pageContext.request.contextPath}/show_clients?page=1"><fmt:message key="admin.menu.client"/> </a></li>
                <li><a href="${pageContext.request.contextPath}/show_orders?page=1"><fmt:message key="admin.menu.orders.show"/></a></li>
            </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
