<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="change_client.title"/></title>
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/cover.css"/>">
</head>
<body>
    <div class="site-wrapper">
        <div class="site-wrapper-inner">
        <div class="cover-container">
            <div class="masthead clearfix">
                <div class="inner">
                    <!--<h3 class="masthead-brand">Main</h3>-->
                    <ul class="nav masthead-nav">
                        <li><a href="${pageContext.request.contextPath}/admin"><fmt:message key="menu.admin.home"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/logout"><fmt:message key="menu.logout"/></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <div class="signup-form">
                            <form action="${pageContext.request.contextPath}/edit_client" method="post" >
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label"><fmt:message key="label.name"/> </label>
                                    <div class="col-sm-10">
                                    <input class="form-control" type="text" id="name" pattern="[a-zA-Zа-яА-я ]{4,100}" name="name" placeholder="<fmt:message key="label.name"/>"
                                           required = "required" value="${authorization.user.name}" title="<fmt:message key="register.validation.name"/>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="login" class="col-sm-2 control-label"><fmt:message key="change_client.login"/></label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" id="login" name="login" placeholder="<fmt:message key="change_client.login"/>" value="${user.login}"
                                               required = "required" pattern="[a-zA-Z0-9_-]{4,16}" title="<fmt:message key="register.validation.login"/>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="group" class="col-sm-2 control-label"><fmt:message key="change_client.role"/></label>
                                    <div class="col-sm-10">
                                        <select id="group" name="group"  class="form-control">
                                            <option>ROLE_USER</option>
                                            <option>ROLE_ADMIN</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="surname" class="col-sm-2 control-label"><fmt:message key="change_client.surname"/></label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" id="surname" pattern="[a-zA-Zа-яА-я ]{4,100}" name="surname" placeholder="<fmt:message key="change_client.surname"/>"
                                               required = "required" value="${authorization.user.surname}" title="<fmt:message key="register.validation.name"/>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-sm-2 control-label"><fmt:message key="change_client.lastname"/></label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" id="lastname" pattern="[a-zA-Zа-яА-я ]{4,100}" name="lastname" placeholder="<fmt:message key="change_client.lastname"/>"
                                               required = "required" value="${authorization.user.surname}" title="<fmt:message key="register.validation.name"/>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="telephone" class="col-sm-2 control-label"><fmt:message key="change_client.telephone"/></label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text"  id ="telephone" name ="telephone" placeholder="<fmt:message key="change_client.telephone"/>"
                                               required = "required" value="${authorization.user.telephone}" pattern="\+[0-9]{12}" title="<fmt:message key="register.validation.telephone"/>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="mail" class="col-sm-2 control-label"><fmt:message key="client.mail"/></label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="text" id="mail" name="mail" placeholder="<fmt:message key="client.mail"/>"
                                               required = "required" value="${authorization.user.mail}"  pattern="[a-zA-Z0-9_]{2,}@[a-zA-z]{2,}\.[a-zA-z]{2,4}" title="<fmt:message key="register.validation.mail"/>"/>
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="${user.id}">
                                <input type="submit" name="Save" value="<fmt:message key="save"/> "/>
                            </form>
                            </div>
                </div>
                    </div>
            <div class="col-sm-3 col-md-2 sidebar">
                <div class="inner">
                    <h3><fmt:message key="admin.menu"/></h3>
                    <ul class="nav nav-list">
                        <li><a href="${pageContext.request.contextPath}/show_demesnes?page=1"><fmt:message key="admin.menu.demesne"/> </a></li>
                        <li><a href="${pageContext.request.contextPath}/add_demesne"><fmt:message key="admin.menu.demesne.add"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/show_clients?page=1"><fmt:message key="admin.menu.client"/> </a></li>
                        <li><a href="${pageContext.request.contextPath}/show_orders?page=1"><fmt:message key="admin.menu.orders.show"/></a></li>

                    </ul>
                </div>
            </div>
    </div>
    </div>
</body>
</html>
