package by.bsuir.demesne.controller;

import by.bsuir.demesne.domain.bean.*;
import by.bsuir.demesne.domain.enumeration.UserRole;
import by.bsuir.demesne.service.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletContext;

/**
 * Created by ����� on 01.05.2016.
 */
@Controller
public class AdminController {
    private static final Logger LOGGER = LogManager.getLogger(AdminController.class);
    private static final Integer PAGE_SIZE = 3;
    private static final Integer DEFAULT_PAGE_NUMBER = 1;

    private static final String LOGIN_PAGE = "login";
    private static final String ADMIN_HOME_PAGE = "admin";
    private static final String ADMIN_ADD_DEMESNE_PAGE = "add_demesne";
    private static final String ADMIN_DEMESNES_PAGE = "demesnes";
    private static final String ADMIN_EDIT_DEMESNE_PAGE = "edit_demesne";
    private static final String ADMIN_EDIT_CLIENT_PAGE ="edit_client";
    private static final String ADMIN_CLIENT_PAGE = "clients";
    private static final String ADMIN_ORDER_PAGE = "orders";
    private static final String ERROR_404_PAGE = "404";
    private static final String ERROR_403_PAGE = "403";

    private static final String LOGGER_MESSAGE = "ServiceException caught";
    private static final String LOGGER_EXCEPTION_MESSAGE = "Exception caught";

    private static final String HOME_URL_MAPPING = "/";
    private static final String LOGIN_URL_MAPPING = "/login";
    private static final String ADMIN_URL_MAPPING = "/admin";
    private static final String INDEX_URL_MAPPING = "/index";
    private static final String SHOWDEMESNES_URL_MAPPING = "/show_demesnes";
    private static final String SHOWCLIENTS_URL_MAPPING = "/show_clients";
    private static final String SHOWORDERS_URL_MAPPING =  "/show_orders";
    private static final String ADDDEMESNE_URL_MAPPING = "/add_demesne";
    private static final String EDITDEMESNE_URL_MAPPING = "/edit_demesne";
    private static final String EDITCLIENT_URL_MAPPING = "/edit_client";
    private static final String EDITORDER_URL_MAPPING = "/edit_order_status";
    private static final String DELETEDEMESNE_URL_MAPPING = "/delete_demesne";
    private static final String DELETECLIENT_URL_MAPPING = "/delete_client";
    private static final String DELETEORDER_URL_MAPPING = "/delete_order";
    private static final String ERROR_404_URL_MAPPING = "/error";
    private static final String ERROR_403_URL_MAPPING = "/403";

    private static final String USER_ATTRIBUTE = "user";
    private static final String DEMESNE_ATTRIBUTE = "demesne";

    private static final String ID_PARAM = "id" ;
    private static final String NAME_PARAM = "name";
    private static final String LOGIN_PARAM = "login";
    private static final String CONFIRM_PARAM = "confirm";
    private static final String ROLE_PARAM = "group";
    private static final String LASTNAME_PARAM = "lastname";
    private static final String SURNAME_PARAM = "surname";
    private static final String TELEPHONE_PARAM = "telephone";
    private static final String MAIL_PARAM = "mail";

    private static final String COST_BUY_PARAM = "cost_toBuy";
    private static final String COST_DAY_PARAM = "cost_perDay";
    private static final String DESCRIPTION_PARAM = "description";

    private static final String PAGE_PAGINATION_PARAM = "page";
    private static final String PAGES_PAGINATION_PARAM = "pages";
    private static final String BEGIN_PAGINATION_PARAM = "beginIndex";
    private static final String END_PAGINATION_PARAM = "endIndex";
    private static final String CURRENT_PAGINATION_PARAM= "currentIndex";
    private static final String CONTENT_PAGINATION_PARAM= "content";

    @Autowired
    DemesneService demesneService;
    @Autowired
    OrderService orderService;
    @Autowired
    UserService userService;
    @Autowired
    AuthorizationService authorizationService;
    @Autowired
    ServletContext servletContext;

    @RequestMapping(value = ERROR_403_URL_MAPPING, method = RequestMethod.GET)
    public ModelAndView error403Page(){
        return new ModelAndView(ERROR_403_PAGE);
    }

    @RequestMapping(value = ERROR_404_URL_MAPPING, method = RequestMethod.GET)
    public ModelAndView errorPage(){
        return new ModelAndView(ERROR_404_PAGE);
    }

    @RequestMapping(value ={HOME_URL_MAPPING,LOGIN_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView start(){
        return new ModelAndView(LOGIN_PAGE);
    }

    @RequestMapping(value = {ADMIN_URL_MAPPING, INDEX_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView adminHomePage(){
        return new ModelAndView(ADMIN_HOME_PAGE);
    }

    @RequestMapping(value = {SHOWDEMESNES_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView showDemesnesPage(@RequestParam(value = PAGE_PAGINATION_PARAM, required = false) Integer pageNumber){
        ModelAndView modelAndView = new ModelAndView(ADMIN_DEMESNES_PAGE);
        try {
            Page<DemesneEntity> page = demesneService.readAll(pageNumber, PAGE_SIZE);
            paginate(modelAndView, page);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE,e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE,exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(value = {SHOWCLIENTS_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView showClientsPage(@RequestParam(value = PAGE_PAGINATION_PARAM, required = false) Integer pageNumber){
        ModelAndView modelAndView = new ModelAndView(ADMIN_CLIENT_PAGE);
        try {
            Page<AuthorizationEntity> page = authorizationService.readAll(pageNumber, PAGE_SIZE);
            paginate(modelAndView, page);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE,exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(value = {SHOWORDERS_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView showOrdersPage(@RequestParam(value = PAGE_PAGINATION_PARAM, required = false) Integer pageNumber){
        ModelAndView modelAndView = new ModelAndView(ADMIN_ORDER_PAGE);
        try {
            Page<OrderEntity> page = orderService.readAll(pageNumber, PAGE_SIZE);
            paginate(modelAndView, page);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE,exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return modelAndView;
    }

    @RequestMapping(value = {ADDDEMESNE_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView addDemesnePage(){
        return new ModelAndView(ADMIN_ADD_DEMESNE_PAGE);
    }

    @RequestMapping(value = {EDITDEMESNE_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView editDemesnePage(@RequestParam(ID_PARAM) Long id){
        ModelAndView modelAndView = new ModelAndView(ADMIN_EDIT_DEMESNE_PAGE);
        DemesneEntity entity = null;
        try {
            entity = demesneService.readById(id);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        modelAndView.addObject(DEMESNE_ATTRIBUTE, entity);
        return modelAndView;
    }

    @RequestMapping(value = {EDITCLIENT_URL_MAPPING},method = RequestMethod.GET)
    public ModelAndView editClientPage(@RequestParam(ID_PARAM) Long id){
        ModelAndView modelAndView = new ModelAndView(ADMIN_EDIT_CLIENT_PAGE);
        AuthorizationEntity entity = null;
        try {
            entity = authorizationService.readById(id);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        modelAndView.addObject(USER_ATTRIBUTE, entity);
        return modelAndView;
    }

    @RequestMapping(value = {EDITORDER_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView editOrderStatusPage(@RequestParam(ID_PARAM) Long id, @RequestParam(CONFIRM_PARAM) Integer confirm){
        try {
            OrderEntity entity = orderService.readById(id);
            entity.setStatus(confirm);
            orderService.update(entity);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE,e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE,exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return showOrdersPage(DEFAULT_PAGE_NUMBER);
    }

    @RequestMapping(value = {DELETEDEMESNE_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView deleteDemesnePage(@RequestParam(ID_PARAM) Long id){
        try {
            demesneService.delete(id);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return showDemesnesPage(DEFAULT_PAGE_NUMBER);
    }

    @RequestMapping(value = {DELETECLIENT_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView deleteClientPage(@RequestParam(ID_PARAM) Long id){
        try {
            authorizationService.delete(id);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return showClientsPage(DEFAULT_PAGE_NUMBER);
    }

    @RequestMapping(value = {DELETEORDER_URL_MAPPING}, method = RequestMethod.GET)
    public ModelAndView deleteOrderPage(@RequestParam(ID_PARAM) Long id){
        try {
            orderService.delete(id);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE,e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE,exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        return showOrdersPage(DEFAULT_PAGE_NUMBER);
    }

    @RequestMapping(value= EDITCLIENT_URL_MAPPING, method = RequestMethod.POST)
    public ModelAndView editClient(@RequestParam(NAME_PARAM) String  name, @RequestParam(LOGIN_PARAM) String  login,
                                   @RequestParam(ROLE_PARAM) String  role, @RequestParam(LASTNAME_PARAM) String  lastname,
                                   @RequestParam(SURNAME_PARAM) String  surname, @RequestParam(TELEPHONE_PARAM) String  telephone,
                                   @RequestParam(MAIL_PARAM) String  mail, @RequestParam(ID_PARAM) Long id){
        try {
            AuthorizationEntity entity = authorizationService.readById(id);
            entity = createUser(entity, login, role, name, surname, lastname, telephone, mail);
            authorizationService.update(entity);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        ModelAndView modelAndView = new ModelAndView();
        RedirectView redirectView = new RedirectView(createRedirectUrl(SHOWCLIENTS_URL_MAPPING));
        modelAndView.setView(redirectView);
        return modelAndView;

    }

    @RequestMapping(value = EDITDEMESNE_URL_MAPPING, method = RequestMethod.POST)
    public ModelAndView editDemesne(@RequestParam(COST_BUY_PARAM) Integer cost_toBuy, @RequestParam(COST_DAY_PARAM) Integer cost_perDay,
                                    @RequestParam(DESCRIPTION_PARAM) String description, @RequestParam(ID_PARAM) Long id){
        try{
            DemesneEntity entity = demesneService.readById(id);
            entity = updateDemesne(entity, cost_toBuy, cost_perDay, description);
            demesneService.update(entity);
        } catch (ServiceException e) {
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        ModelAndView modelAndView = new ModelAndView();
        RedirectView redirectView = new RedirectView(createRedirectUrl(SHOWDEMESNES_URL_MAPPING));
        modelAndView.setView(redirectView);
        return modelAndView;
    }

    @RequestMapping(value = ADDDEMESNE_URL_MAPPING, method = RequestMethod.POST)
    public ModelAndView addDemesne(@RequestParam(NAME_PARAM) String name,
                                   @RequestParam(COST_BUY_PARAM) Integer cost_toBuy, @RequestParam(COST_DAY_PARAM) Integer cost_perDay,
                                   @RequestParam(DESCRIPTION_PARAM) String description){
        try{
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principal = authentication.getPrincipal();
            String username;
            if (principal instanceof UserDetails) {
                username = ((UserDetails)principal).getUsername();
            } else {
                username = principal.toString();
            }

            UserEntity user = userService.readById(authorizationService.getUser(username).getId());
            DemesneEntity entity = createDemesne(new DemesneEntity(), name, cost_toBuy, cost_perDay, description, user);
            System.out.println(entity.toString());
            demesneService.add(entity);
        }catch (ServiceException e){
            System.out.println(e);
            LOGGER.warn(LOGGER_MESSAGE, e);
        }
        catch (Exception exception){
            System.out.println(exception);
            LOGGER.warn(LOGGER_EXCEPTION_MESSAGE, exception);
            return new ModelAndView(ERROR_404_PAGE);
        }
        ModelAndView modelAndView = new ModelAndView();
        RedirectView redirectView = new RedirectView(createRedirectUrl(SHOWDEMESNES_URL_MAPPING));
        modelAndView.setView(redirectView);
        return modelAndView;
    }

    private void paginate(ModelAndView modelAndView, Page page){
        int current = page.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, page.getTotalPages());
        modelAndView.addObject(PAGES_PAGINATION_PARAM, page);
        modelAndView.addObject(CONTENT_PAGINATION_PARAM, page.getContent());
        modelAndView.addObject(BEGIN_PAGINATION_PARAM, begin);
        modelAndView.addObject(END_PAGINATION_PARAM, end);
        modelAndView.addObject(CURRENT_PAGINATION_PARAM, current);
    }
    private String createRedirectUrl(String url){
        return servletContext.getContextPath() + url + "?" + PAGE_PAGINATION_PARAM + "=" + DEFAULT_PAGE_NUMBER;
    }
    private AuthorizationEntity createUser(AuthorizationEntity entity, String login, String role, String name, String surname, String lastname, String telephone, String mail){
        entity.setLogin(login);
        entity.setGroup(UserRole.valueOf(role.toUpperCase()));
        UserEntity client = entity.getUserEntity();
        client.setName(name);
        client.setSurname(surname);
        client.setLastname(lastname);
        client.setTelephone(telephone);
        client.setMail(mail);
        entity.setUserEntity(client);
        return entity;
    }

    private DemesneEntity createDemesne(DemesneEntity entity , String name, Integer cost_toBuy, Integer cost_perDay, String description, UserEntity user){
        TypeDemesneEntity typeDemesneEntity = new TypeDemesneEntity();
        typeDemesneEntity.setName(name);
        entity.setTypeDemesneEntity(typeDemesneEntity);
        entity.setCost_toBuy(cost_toBuy);
        entity.setCost_perDay(cost_perDay);
        entity.setId_user(user);
        entity.setPhoto("0");
        entity.setDescription(description);
        entity.setAccess(1);
        return entity;
    }

    private DemesneEntity updateDemesne(DemesneEntity entity, Integer cost_toBuy, Integer cost_perDay, String description){
        entity.setCost_toBuy(cost_toBuy);
        entity.setCost_perDay(cost_perDay);
        entity.setDescription(description);
        return entity;
    }
}
