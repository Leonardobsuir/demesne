<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<html>
<head>
    <title><fmt:message key="user.login.title"/> </title>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/cover.css"/>">
</head>
<body>
<div class="site-wrapper">
    <div class="site-wrapper-inner">
        <div class="cover-container">
            <div class="masthead clearfix">
                <div class="inner">
                </div> <ul class="nav masthead-nav">
                <li><a href="${pageContext.request.contextPath}/login?language=ru"><fmt:message key="menu.ru"/></a></li>
                <li><a href="${pageContext.request.contextPath}/login?language=en"><fmt:message key="menu.en"/></a></li>
            </ul>
            </div>
            <div class="inner cover">
                <section id="form">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-1">
                                <h2 class="login-form heading"><fmt:message key="login.message"/> </h2>
                                <div class="login-form">
                                    <c:url value="/j_spring_security_check" var="loginUrl" />
                                    <form name="login-form" action="${loginUrl}" method="post" >
                                        <div class="top-margin">
                                            <label><fmt:message key="change_client.login"/></label>
                                            <input type="text" class="form-control" name="j_username">
                                        </div>
                                        <div class="top-margin">
                                            <label><fmt:message key="label.password"/></label>
                                            <input type="password" class="form-control" name="j_password">
                                        </div>

                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-4 text-right">
                                                <button class="btn btn-action" type="submit"><fmt:message key="signin"/></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#html5Form').formValidation();
    });
</script>
</body>
</html>