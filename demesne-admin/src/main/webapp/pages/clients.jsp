<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<html>
<head>
    <title><fmt:message key="client.title"/></title>
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/cover.css"/>">

</head>
<body>
<div class="site-wrapper">
    <div class="site-wrapper-inner">
        <div class="cover-container">
            <div class="masthead clearfix">
                <div class="inner">
                    <ul class="nav masthead-nav">
                        <li><a href="${pageContext.request.contextPath}/admin"><fmt:message key="menu.admin.home"/></a> </li>
                        <li><a href="${pageContext.request.contextPath}/logout"><fmt:message key="menu.logout"/></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
              <h2><fmt:message key="client.h"/></h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><fmt:message key="label.name"/></th>
                        <th><fmt:message key="client.mail"/></th>
                        <th colspan="2"><fmt:message key="demesnes.label.action"/></th>
                    </tr>
                    </thead>
                    <tfoot>
                    <td colspan="8">
                    <c:url var="firstUrl" value="/show_clients?page=1" />
                    <c:url var="lastUrl" value="/show_clients?page=${pages.totalPages}" />
                    <c:url var="prevUrl" value="/show_clients?page=${currentIndex - 1}" />
                    <c:url var="nextUrl" value="/show_clients?page=${currentIndex + 1}" />
                        <ul class="pagination">
                            <c:choose>
                                <c:when test="${currentIndex == 1}">
                                    <li class="disabled"><a href="#">&lt;&lt;</a></li>
                                    <li class="disabled"><a href="#">&lt;</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li><a href="${firstUrl}">&lt;&lt;</a></li>
                                    <li><a href="${prevUrl}">&lt;</a></li>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
                                <c:url var="pageUrl" value="/show_clients?page=${i}" />
                                <c:choose>
                                    <c:when test="${i == currentIndex}">
                                        <li class="active"><a href="${pageUrl}"><c:out value="${i}" /></a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${currentIndex == pages.totalPages}">
                                    <li class="disabled"><a href="#">&gt;</a></li>
                                    <li class="disabled"><a href="#">&gt;&gt;</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li><a href="${nextUrl}">&gt;</a></li>
                                    <li><a href="${lastUrl}">&gt;&gt;</a></li>
                                </c:otherwise>
                            </c:choose>
                        </ul>
                    </td>
                    </tfoot>
                    <tbody>
                        <c:forEach var="item" items="${content}">
                        <tr>
                            <td>${item.id}</td>
                            <td>${item.userEntity.name}</td>
                            <td>${item.userEntity.mail}</td>
                            <td>
                                <a href="${pageContext.request.contextPath}/edit_client?id=${item.id}"><fmt:message key="demesnes.button.change"/></a>
                            </form>
                            </td>
                            <td>
                                <a href="${pageContext.request.contextPath}/delete_client?id=${item.id}"><fmt:message key="demesnes.button.delete"/></a>
                            </td>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-3 col-md-2 sidebar">
            <div class="inner">
                <h3><fmt:message key="admin.menu"/></h3>
                <ul class="nav nav-list">
                    <li><a href="${pageContext.request.contextPath}/show_cars?page=1"><fmt:message key="admin.menu.demesne"/> </a></li>
                    <li><a href="${pageContext.request.contextPath}/add_car"><fmt:message key="admin.menu.demesne.add"/></a></li>
                    <li><a href="${pageContext.request.contextPath}/show_clients?page=1"><fmt:message key="admin.menu.client"/> </a></li>
                    <li><a href="${pageContext.request.contextPath}/show_orders?page=1"><fmt:message key="admin.menu.orders.show"/></a></li>

                </ul>
            </div>
        </div>
    </div>
    </div>
</div>
</body>
</html>
