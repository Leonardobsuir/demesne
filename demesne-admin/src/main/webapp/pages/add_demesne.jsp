<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="add_demesne.title"/></title>
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/pages/assets/css/cover.css"/>">
</head>

<body>

<div class="site-wrapper">
    <div class="site-wrapper-inner">
        <div class="cover-container">
            <div class="masthead clearfix">
                <div class="inner">
                    <!--<h3 class="masthead-brand">Main</h3>-->
                    <ul class="nav masthead-nav">
                        <ul class="nav masthead-nav">
                            <li><a href="admin"><fmt:message key="menu.admin.home"/></a> </li>
                            <li><a href="${pageContext.request.contextPath}/logout"><fmt:message key="menu.logout"/></a></li>
                        </ul>
                    </ul>
                </div>
            </div>
                        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                            <c:if test="${error eq true}">
                                <h4><fmt:message key="incorrect.params"/> </h4>
                            </c:if>
                            <div class="signup-form">
                                <form class="form-signin" action="${pageContext.request.contextPath}/add_demesne" method="post" role="form">

                                    <div class="form-group">
                                        <label for="name" class="col-sm-3 control-label"><fmt:message key="demesnes.name"/> </label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" id="name" name ="name" placeholder="<fmt:message key="demesnes.name"/>" required="required"
                                                  pattern="[a-zA-Zа-яА-Я ]{3,63}" title="<fmt:message key="add_demesne.name.message"/>"/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cost_toBuy" class="col-sm-3 control-label"><fmt:message key="demesnes.cost_toBuy"/></label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="number" id="cost_toBuy" name ="cost_toBuy" placeholder="<fmt:message key="demesnes.cost_toBuy"/>"  required="required"
                                                   min="1" title="<fmt:message key="add_demesne.costBuy.message"/>"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cost_perDay" class="col-sm-3 control-label"><fmt:message key="demesnes.cost_perDay"/></label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="number" id="cost_perDay" name ="cost_perDay" placeholder="<fmt:message key="demesnes.cost_perDay"/>"  required="required"
                                                   min="1" title="<fmt:message key="add_demesne.costDay.message"/>"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="description" class="col-sm-3 control-label"><fmt:message key="add_demesne.description"/></label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" id="description" name ="description" placeholder="<fmt:message key="add_demesne.description"/>"  required="required"
                                                   pattern="[a-zA-Zа-яА-Я0-9 ]{1,63}" title="<fmt:message key="add_demesne.description.message"/>"/>
                                        </div>
                                    </div>
                                    <input type="submit" name="Save" value="<fmt:message key="save"/>"/>

                                </form>
                            </div>
                        </div>
        </div>
        <div class="col-sm-3 col-md-2 sidebar">
            <div class="inner">
                <h3><fmt:message key="admin.menu"/></h3>
                <ul class="nav nav-list">
                    <li><a href="${pageContext.request.contextPath}/show_demesnes?page=1"><fmt:message key="admin.menu.demesne"/> </a></li>
                    <li><a href="${pageContext.request.contextPath}/add_demesne"><fmt:message key="admin.menu.demesne.add"/></a></li>
                    <li><a href="${pageContext.request.contextPath}/show_clients?page=1"><fmt:message key="admin.menu.client"/> </a></li>
                    <li><a href="${pageContext.request.contextPath}/show_orders?page=1"><fmt:message key="admin.menu.orders.show"/></a></li>

                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
