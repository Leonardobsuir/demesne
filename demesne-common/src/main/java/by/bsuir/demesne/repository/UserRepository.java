package by.bsuir.demesne.repository;

import by.bsuir.demesne.domain.bean.AuthorizationEntity;
import by.bsuir.demesne.domain.bean.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by ������� on 30/04/2016.
 */
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    @Query("SELECT user FROM UserEntity user WHERE user.name=:name")
    AuthorizationEntity findByName(@Param("name")String name);
}
