package by.bsuir.demesne.repository;

import by.bsuir.demesne.domain.bean.AuthorizationEntity;
import jdk.nashorn.internal.ir.IdentNode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ������� on 30/04/2016.
 */
public interface AuthorizationRepository extends JpaRepository<AuthorizationEntity, Long> {
    AuthorizationEntity findByLogin(String login);
}
