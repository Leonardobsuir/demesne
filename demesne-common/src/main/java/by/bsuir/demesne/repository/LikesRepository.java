package by.bsuir.demesne.repository;

import by.bsuir.demesne.domain.bean.LikesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ������� on 30/04/2016.
 */
public interface LikesRepository extends JpaRepository<LikesEntity, Long> {
}
