package by.bsuir.demesne.repository;

import by.bsuir.demesne.domain.bean.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ������� on 30/04/2016.
 */
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
}
