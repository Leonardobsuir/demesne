package by.bsuir.demesne.service.impl;

import by.bsuir.demesne.domain.bean.TypeOfOrderEntity;
import by.bsuir.demesne.repository.TypeOfOrderRepository;
import by.bsuir.demesne.service.ServiceException;
import by.bsuir.demesne.service.TypeOfOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ������� on 01/05/2016.
 */
@Service
@Scope(scopeName = "singleton")
@Transactional(rollbackFor = ServiceException.class)
public class TypeOfOrderServiceImpl implements TypeOfOrderService {

    @Autowired
    TypeOfOrderRepository typeOfOrderRepository;

    private static TypeOfOrderServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private TypeOfOrderServiceImpl() {

    }

    @Override
    public TypeOfOrderEntity add(TypeOfOrderEntity entity) throws ServiceException {
        return typeOfOrderRepository.saveAndFlush(entity);
    }

    @Override
    public TypeOfOrderEntity readById(Long id) throws ServiceException {
        return typeOfOrderRepository.findOne(id);
    }

    @Override
    public void delete(Long id) throws ServiceException {
        typeOfOrderRepository.delete(id);
    }

    @Override
    public TypeOfOrderEntity update(TypeOfOrderEntity entity) throws ServiceException {
        return typeOfOrderRepository.saveAndFlush(entity);
    }

    @Override
    public Page<TypeOfOrderEntity> readAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        PageRequest request = new PageRequest(pageNumber-1,pageSize);
        return typeOfOrderRepository.findAll(request);
    }

    private static class SingletonHolder {
        private static final TypeOfOrderServiceImpl INSTANCE = new TypeOfOrderServiceImpl();
    }
}
