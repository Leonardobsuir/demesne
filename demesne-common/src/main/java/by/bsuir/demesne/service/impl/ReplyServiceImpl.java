package by.bsuir.demesne.service.impl;

import by.bsuir.demesne.domain.bean.ReplyEntity;
import by.bsuir.demesne.repository.ReplyRepository;
import by.bsuir.demesne.service.ReplyService;
import by.bsuir.demesne.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ������� on 01/05/2016.
 */
@Service
@Scope(scopeName = "singleton")
@Transactional(rollbackFor = ServiceException.class)
public class ReplyServiceImpl implements ReplyService {

    @Autowired
    ReplyRepository replyRepository;

    private static ReplyServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private ReplyServiceImpl() {

    }

    @Override
    public ReplyEntity add(ReplyEntity entity) throws ServiceException {
        return replyRepository.saveAndFlush(entity);
    }

    @Override
    public ReplyEntity readById(Long id) throws ServiceException {
        return replyRepository.findOne(id);
    }

    @Override
    public void delete(Long id) throws ServiceException {
        replyRepository.delete(id);
    }

    @Override
    public ReplyEntity update(ReplyEntity entity) throws ServiceException {
        return replyRepository.saveAndFlush(entity);
    }

    @Override
    public Page<ReplyEntity> readAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        PageRequest request = new PageRequest(pageNumber-1,pageSize);
        return replyRepository.findAll(request);
    }

    private static class SingletonHolder {
        private static final ReplyServiceImpl INSTANCE = new ReplyServiceImpl();
    }
}
