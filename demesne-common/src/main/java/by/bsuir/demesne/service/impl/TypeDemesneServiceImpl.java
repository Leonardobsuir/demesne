package by.bsuir.demesne.service.impl;

import by.bsuir.demesne.domain.bean.TypeDemesneEntity;
import by.bsuir.demesne.repository.TypeDemesneRepository;
import by.bsuir.demesne.service.ServiceException;
import by.bsuir.demesne.service.TypeDemesneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ������� on 01/05/2016.
 */
@Service
@Scope(scopeName = "singleton")
@Transactional(rollbackFor = ServiceException.class)
public class TypeDemesneServiceImpl implements TypeDemesneService {

    @Autowired
    TypeDemesneRepository typeDemesneRepository;

    private static TypeDemesneServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private TypeDemesneServiceImpl() {

    }

    @Override
    public TypeDemesneEntity add(TypeDemesneEntity entity) throws ServiceException {
        return typeDemesneRepository.saveAndFlush(entity);
    }

    @Override
    public TypeDemesneEntity readById(Long id) throws ServiceException {
        return typeDemesneRepository.findOne(id);
    }

    @Override
    public void delete(Long id) throws ServiceException {
        typeDemesneRepository.delete(id);
    }

    @Override
    public TypeDemesneEntity update(TypeDemesneEntity entity) throws ServiceException {
        return typeDemesneRepository.saveAndFlush(entity);
    }

    @Override
    public Page<TypeDemesneEntity> readAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        PageRequest request = new PageRequest(pageNumber-1,pageSize);
        return typeDemesneRepository.findAll(request);
    }

    private static class SingletonHolder {
        private static final TypeDemesneServiceImpl INSTANCE = new TypeDemesneServiceImpl();
    }
}
