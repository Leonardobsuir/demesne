package by.bsuir.demesne.service;

import by.bsuir.demesne.domain.bean.AuthorizationEntity;

/**
 * Created by ������� on 30/04/2016.
 */
public interface AuthorizationService extends GenericService<AuthorizationEntity, Long> {
    AuthorizationEntity getUser(String login) throws ServiceException;
    AuthorizationEntity create(AuthorizationEntity authorizationEntity) throws ServiceException;
}
