package by.bsuir.demesne.service.impl;

import by.bsuir.demesne.domain.bean.UserEntity;
import by.bsuir.demesne.repository.UserRepository;
import by.bsuir.demesne.service.ServiceException;
import by.bsuir.demesne.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ������� on 01/05/2016.
 */
@Service
@Scope(scopeName = "singleton")
@Transactional(rollbackFor = ServiceException.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private UserServiceImpl() {
    }

    public static UserServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public UserEntity add(UserEntity entity) throws ServiceException {
        return userRepository.saveAndFlush(entity);
    }

    @Override
    public UserEntity readById(Long id) throws ServiceException {
        return userRepository.findOne(id);
    }

    @Override
    public void delete(Long id) throws ServiceException {
        userRepository.delete(id);
    }

    @Override
    public UserEntity update(UserEntity entity) throws ServiceException {
        return userRepository.saveAndFlush(entity);
    }

    @Override
    public Page<UserEntity> readAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        PageRequest request = new PageRequest(pageNumber-1,pageSize);
        return userRepository.findAll(request);
    }

    private static class SingletonHolder {
        private static final UserServiceImpl INSTANCE = new UserServiceImpl();
    }
}
