package by.bsuir.demesne.service.impl;

import by.bsuir.demesne.domain.bean.LikesEntity;
import by.bsuir.demesne.repository.LikesRepository;
import by.bsuir.demesne.service.LikesService;
import by.bsuir.demesne.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ������� on 01/05/2016.
 */
@Service
@Scope(scopeName = "singleton")
@Transactional(rollbackFor = ServiceException.class)
public class LikesServiceImpl implements LikesService {

    @Autowired
    LikesRepository likesRepository;

    private static LikesServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private LikesServiceImpl() {

    }

    @Override
    public LikesEntity add(LikesEntity entity) throws ServiceException {
        return likesRepository.saveAndFlush(entity);
    }

    @Override
    public LikesEntity readById(Long id) throws ServiceException {
        return likesRepository.findOne(id);
    }

    @Override
    public void delete(Long id) throws ServiceException {
        likesRepository.delete(id);
    }

    @Override
    public LikesEntity update(LikesEntity entity) throws ServiceException {
        return likesRepository.saveAndFlush(entity);
    }

    @Override
    public Page<LikesEntity> readAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        PageRequest request = new PageRequest(pageNumber-1,pageSize);
        return likesRepository.findAll(request);
    }

    private static class SingletonHolder {
        private static final LikesServiceImpl INSTANCE = new LikesServiceImpl();
    }
}
