package by.bsuir.demesne.service.impl;

import by.bsuir.demesne.domain.bean.DemesneEntity;
import by.bsuir.demesne.repository.DemesneRepository;
import by.bsuir.demesne.service.DemesneService;
import by.bsuir.demesne.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ������� on 01/05/2016.
 */
@Service
@Scope(scopeName = "singleton")
@Transactional(rollbackFor = ServiceException.class)
public class DemesneServiceImpl implements DemesneService {

    @Autowired
    DemesneRepository demesneRepository;

    private static DemesneServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private DemesneServiceImpl() {

    }

    @Override
    public DemesneEntity add(DemesneEntity demesneEntity) throws ServiceException {
        return demesneRepository.saveAndFlush(demesneEntity);
    }

    @Override
    public DemesneEntity readById(Long id) throws ServiceException {
        return demesneRepository.findOne(id);
    }

    @Override
    public void delete(Long id) throws ServiceException {
        demesneRepository.delete(id);
    }

    @Override
    public DemesneEntity update(DemesneEntity demesneEntity) throws ServiceException {
        return demesneRepository.saveAndFlush(demesneEntity);
    }

    @Override
    public Page<DemesneEntity> readAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        PageRequest request = new PageRequest(pageNumber-1,pageSize);
        return demesneRepository.findAll(request);
    }

    private static class SingletonHolder {
        private static final DemesneServiceImpl INSTANCE = new DemesneServiceImpl();
    }
}
