package by.bsuir.demesne.service;

import by.bsuir.demesne.domain.bean.ReplyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ������� on 30/04/2016.
 */
public interface ReplyService extends GenericService<ReplyEntity, Long> {
}
