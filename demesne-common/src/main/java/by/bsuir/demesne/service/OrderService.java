package by.bsuir.demesne.service;

import by.bsuir.demesne.domain.bean.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ������� on 30/04/2016.
 */
public interface OrderService extends GenericService<OrderEntity, Long> {
}
