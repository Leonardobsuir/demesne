package by.bsuir.demesne.service;

import by.bsuir.demesne.domain.bean.TypeOfOrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ������� on 30/04/2016.
 */
public interface TypeOfOrderService extends GenericService<TypeOfOrderEntity, Long> {
}
