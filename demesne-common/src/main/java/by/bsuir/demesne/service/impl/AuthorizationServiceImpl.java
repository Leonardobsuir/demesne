package by.bsuir.demesne.service.impl;

import by.bsuir.demesne.domain.bean.AuthorizationEntity;
import by.bsuir.demesne.repository.AuthorizationRepository;
import by.bsuir.demesne.service.AuthorizationService;
import by.bsuir.demesne.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Scope(scopeName = "singleton")
@Transactional(rollbackFor = ServiceException.class)
public class AuthorizationServiceImpl implements AuthorizationService {

    @Autowired
    AuthorizationRepository authorizationRepository;

    private static AuthorizationServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private AuthorizationServiceImpl() {

    }

    @Override
    public AuthorizationEntity add(AuthorizationEntity entity) throws ServiceException {
        return authorizationRepository.saveAndFlush(entity);
    }

    @Override
    public AuthorizationEntity readById(Long id) throws ServiceException {
        return authorizationRepository.findOne(id);
    }

    @Override
    public void delete(Long id) throws ServiceException {
        authorizationRepository.delete(id);
    }

    @Override
    public AuthorizationEntity update(AuthorizationEntity entity) throws ServiceException {
        return authorizationRepository.saveAndFlush(entity);
    }

    @Override
    public Page<AuthorizationEntity> readAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        PageRequest request = new PageRequest(pageNumber-1,pageSize);
        return authorizationRepository.findAll(request);
    }

    @Override
    public AuthorizationEntity getUser(String login) {
        AuthorizationEntity checkedUser=null;
        AuthorizationEntity uncheckedUser = authorizationRepository.findByLogin(login);
        if (uncheckedUser!=null) {
            checkedUser=uncheckedUser;
        }
        return checkedUser;

    }

    @Override
    public AuthorizationEntity create(AuthorizationEntity entity){
        return authorizationRepository.saveAndFlush(entity);
    }

    private static class SingletonHolder {
        private static final AuthorizationServiceImpl INSTANCE = new AuthorizationServiceImpl();
    }
}
