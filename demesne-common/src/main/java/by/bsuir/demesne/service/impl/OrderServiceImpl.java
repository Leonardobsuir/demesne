package by.bsuir.demesne.service.impl;

import by.bsuir.demesne.domain.bean.OrderEntity;
import by.bsuir.demesne.repository.OrderRepository;
import by.bsuir.demesne.service.OrderService;
import by.bsuir.demesne.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ������� on 01/05/2016.
 */
@Service
@Scope(scopeName = "singleton")
@Transactional(rollbackFor = ServiceException.class)
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    private static OrderServiceImpl getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private OrderServiceImpl() {

    }

    @Override
    public OrderEntity add(OrderEntity entity) throws ServiceException {
        return orderRepository.saveAndFlush(entity);
    }

    @Override
    public OrderEntity readById(Long id) throws ServiceException {
        return orderRepository.findOne(id);
    }

    @Override
    public void delete(Long id) throws ServiceException {
        orderRepository.delete(id);
    }

    @Override
    public OrderEntity update(OrderEntity entity) throws ServiceException {
        return orderRepository.saveAndFlush(entity);
    }

    @Override
    public Page<OrderEntity> readAll(Integer pageNumber, Integer pageSize) throws ServiceException {
        PageRequest request = new PageRequest(pageNumber-1,pageSize);
        return orderRepository.findAll(request);
    }

    private static class SingletonHolder {
        private static final OrderServiceImpl INSTANCE = new OrderServiceImpl();
    }
}
