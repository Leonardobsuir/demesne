package by.bsuir.demesne.service;

/**
 * Created by ������� on 30/04/2016.
 */

import org.springframework.data.domain.Page;

/**
 * Declares common actions for objects types T.
 *
 * @param <T> Generic type of objects passed to methods
 * @param <K> Generic type of objects which will serve as a key
 */
public interface GenericService<T, K> {
    /**
     * Add object type T into system.
     *
     * @param entity Object type T which is need to be added to the system
     * @throws ServiceException if there was an errors on the lower level.
     *                          //* @throws ValidationException if fields of T object are not valid.
     */
    T add(T entity) throws ServiceException;

    /**
     * Finds object type T with id specified as first parameter.
     *
     * @param id If of user
     * @return T object with not null fields if user exists.T with null fields if user not exists in system.
     * @throws ServiceException if there was an errors on the lower level.
     */
    T readById(K id) throws ServiceException;

    /**
     * Deletes object type T with id specified as paramater from system.
     *
     * @param id Id of object Type T which is need to be deleted.
     * @throws ServiceException if there was an errors on the lower level.
     */
    void delete(K id) throws ServiceException;

    /**
     * Updates data in system for object type T from object specified as paramter.
     *
     * @param entity Object type T that contain new data.
     * @throws ServiceException      if there was an errors on the lower level.
     * @throws //ValidationException if fields of T object are not valid.
     */
    T update(T entity) throws ServiceException;

    Page<T> readAll(Integer pageNumber, Integer pageSize) throws ServiceException;
}

