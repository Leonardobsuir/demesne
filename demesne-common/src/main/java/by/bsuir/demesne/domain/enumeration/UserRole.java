package by.bsuir.demesne.domain.enumeration;


public enum UserRole {

    ROLE_ADMIN,
    ROLE_USER,
    ANONYMOUS;

    UserRole() {
    }

}