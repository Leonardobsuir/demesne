package by.bsuir.demesne.domain.bean;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by ������� on 30/04/2016.
 */
@Entity
@Table(name = "type_demesne", schema = "", catalog = "kursach")
public class TypeDemesneEntity {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment", strategy= "increment")
    @Column(name = "`id`")
    private Long id;
    @Column(name = "`name`")
    private String name;

    public TypeDemesneEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TypeDemesneEntity that = (TypeDemesneEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        Long result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return Integer.parseInt(result.toString());
    }

    @Override
    public String toString() {
        return "TypeDemesneEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
