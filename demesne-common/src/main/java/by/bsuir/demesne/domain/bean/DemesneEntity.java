package by.bsuir.demesne.domain.bean;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by ������� on 30/04/2016.
 */
@Entity
@Table(name = "demesne", schema = "", catalog = "kursach")
public class DemesneEntity {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "`id`")
    private Long id;
    @Column(name = "`cost_toBuy`")
    private int cost_toBuy;
    @Column(name = "`cost_perDay`")
    private int cost_perDay;
    @Column(name = "`photo`")
    private String photo;
    @Column(name = "`description`")
    private String description;
    @Column(name = "`access`")
    private int access;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "`id_type_Demesne`")
    private TypeDemesneEntity typeDemesneEntity;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "`id_user`")
    private UserEntity id_user;

    public DemesneEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCost_toBuy() {
        return cost_toBuy;
    }

    public void setCost_toBuy(int cost_toBuy) {
        this.cost_toBuy = cost_toBuy;
    }

    public int getCost_perDay() {
        return cost_perDay;
    }

    public void setCost_perDay(int cost_perDay) {
        this.cost_perDay = cost_perDay;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TypeDemesneEntity getTypeDemesneEntity() {
        return typeDemesneEntity;
    }

    public void setTypeDemesneEntity(TypeDemesneEntity typeDemesneEntity) {
        this.typeDemesneEntity = typeDemesneEntity;
    }

    public UserEntity getId_user() {
        return id_user;
    }

    public void setId_user(UserEntity id_user) {
        this.id_user = id_user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DemesneEntity that = (DemesneEntity) o;

        if (id != that.id) return false;
        if (cost_toBuy != that.cost_toBuy) return false;
        if (cost_perDay != that.cost_perDay) return false;
        if (photo != null ? !photo.equals(that.photo) : that.photo != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        Long result = id;
        result = 31 * result + cost_toBuy;
        result = 31 * result + cost_perDay;
        result = 31 * result + (photo != null ? photo.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return Integer.parseInt(result.toString());
    }

    @Override
    public String toString() {
        return "DemesneEntity{" +
                "id=" + id +
                ", cost_toBuy=" + cost_toBuy +
                ", cost_perDay=" + cost_perDay +
                ", photo='" + photo + '\'' +
                ", description='" + description + '\'' +
                ", typeDemesneEntity=" + typeDemesneEntity +
                ", id_user=" + id_user +
                '}';
    }
}
