package by.bsuir.demesne.domain.bean;

import by.bsuir.demesne.domain.enumeration.UserRole;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by ������� on 30/04/2016.
 */
@Entity
@Table(name = "authorization", schema = "", catalog = "kursach")
public class AuthorizationEntity {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "`id`")
    private Long id;
    @Column(name = "`group`")
    @Enumerated(EnumType.STRING)
    private UserRole group;
    @Column(name = "`login`")
    private String login;
    @Column(name = "`password`")
    private String password;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "`id_user`")
    private UserEntity id_user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRole getGroup() {
        return group;
    }

    public void setGroup(UserRole group) {
        this.group = group;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public UserEntity getUserEntity() {
        return id_user;
    }

    public void setUserEntity(UserEntity id_user) {
        this.id_user = id_user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthorizationEntity that = (AuthorizationEntity) o;

        if (id != that.id) return false;
        if (group != that.group) return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        return !(password != null ? !password.equals(that.password) : that.password != null);

    }

    @Override
    public int hashCode() {
        Long result = id;
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return Integer.parseInt(result.toString());
    }

    @Override
    public String toString() {
        return "AuthorizationEntity{" +
                "id=" + id +
                ", group=" + group +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", id_user=" + id_user +
                '}';
    }
}
