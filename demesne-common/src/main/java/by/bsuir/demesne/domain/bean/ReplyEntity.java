package by.bsuir.demesne.domain.bean;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by ������� on 30/04/2016.
 */
@Entity
@Table(name = "reply", schema = "", catalog = "kursach")
public class ReplyEntity {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name= "increment", strategy= "increment")
    @Column(name = "`id`")
    private Long id;
    @Column(name = "`text`")
    private String text;
    @Column(name = "`likes`")
    private Integer likes;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "`id_demesne`")
    private DemesneEntity id_demesne;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "`id_user`")
    private UserEntity id_user;

    public ReplyEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public DemesneEntity getId_demesne() {
        return id_demesne;
    }

    public void setId_demesne(DemesneEntity id_demesne) {
        this.id_demesne = id_demesne;
    }

    public UserEntity getId_user() {
        return id_user;
    }

    public void setId_user(UserEntity id_user) {
        this.id_user = id_user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReplyEntity that = (ReplyEntity) o;

        if (id != that.id) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (likes != null ? !likes.equals(that.likes) : that.likes != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        Long result = id;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (likes != null ? likes.hashCode() : 0);
        return Integer.parseInt(result.toString());
    }

    @Override
    public String toString() {
        return "ReplyEntity{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", likes=" + likes +
                ", id_demesne=" + id_demesne +
                ", id_user=" + id_user +
                '}';
    }
}
