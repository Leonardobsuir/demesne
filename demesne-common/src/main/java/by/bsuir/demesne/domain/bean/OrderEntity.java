package by.bsuir.demesne.domain.bean;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by ������� on 30/04/2016.
 */
@Entity
@Table(name = "`order`", schema = "", catalog = "kursach")
public class OrderEntity {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "`id`")
    private Long id;
    @Column(name = "`days`")
    private int days;
    @Column(name = "`status`")
    private Integer status;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "`id_user`")
    private UserEntity id_user;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "`id_demesne`")
    private DemesneEntity id_demesne;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "`id_type_of_order`")
    private TypeOfOrderEntity id_type_of_order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public UserEntity getId_user() {
        return id_user;
    }

    public void setId_user(UserEntity id_user) {
        this.id_user = id_user;
    }

    public TypeOfOrderEntity getId_type_of_order() {
        return id_type_of_order;
    }

    public void setId_type_of_order(TypeOfOrderEntity id_type_of_order) {
        this.id_type_of_order = id_type_of_order;
    }

    public DemesneEntity getId_demesne() {
        return id_demesne;
    }

    public void setId_demesne(DemesneEntity id_demesne) {
        this.id_demesne = id_demesne;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderEntity that = (OrderEntity) o;

        if (id != that.id) return false;
        if (days != that.days) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        Long result = id;
        result = 31 * result + days;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return Integer.parseInt(result.toString());
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
                "id=" + id +
                ", days=" + days +
                ", status=" + status +
                ", id_user=" + id_user +
                ", id_demesne=" + id_demesne +
                ", id_type_of_order=" + id_type_of_order +
                '}';
    }
}
