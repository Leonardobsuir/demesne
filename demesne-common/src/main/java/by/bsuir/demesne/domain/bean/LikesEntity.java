package by.bsuir.demesne.domain.bean;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by ������� on 30/04/2016.
 */
@Entity
@Table(name = "likes", schema = "", catalog = "kursach")
public class LikesEntity {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "`id`")
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "`id_user`")
    private UserEntity id_user;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "`id_reply`")
    private ReplyEntity id_reply;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserEntity getId_user() {
        return id_user;
    }

    public void setId_user(UserEntity id_user) {
        this.id_user = id_user;
    }

    public ReplyEntity getId_reply() {
        return id_reply;
    }

    public void setId_reply(ReplyEntity id_reply) {
        this.id_reply = id_reply;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LikesEntity that = (LikesEntity) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(id.toString());
    }

    @Override
    public String toString() {
        return "LikesEntity{" +
                "id=" + id +
                ", id_user=" + id_user +
                ", id_reply=" + id_reply +
                '}';
    }
}
